<p align="center"><img src="qfib.png" width="200" /></p>

# Overview
This repository contains some example data for the paper: 

>**QFib: Fast and Efficient Brain Tractogram Compression** *Corentin Mercier, Sylvain Rousseau, Pietro Gori, Isabelle Bloch and Tamy Boubekeur.*

whose reference implementation can be found here:
https://github.com/syrousseau/qfib

Copyright(C) 2020 Corentin Mercier, Sylvain Rousseau, Pietro Gori, Isabelle Bloch and Tamy Boubekeur

All right reserved

# Data description
The tck files contains what their names describe.
For instance, tracto60kiFOD10.1.tck means that the file contains 60.000 streamlines, obtained using the iFOD1 algorithm of mrtrix3 with a step size of 0.1mm.

All data is built using mrtrix3 with the algorithms iFOD1 and SD-STREAM.
Original MRI data come from the Human Connectome Project (http://www.humanconnectomeproject.org).

The data comes from the 3 Tesla scanner : (https://www.humanconnectome.org/study/hcp-young-adult/document/1200-subjects-data-release). 
We forced the fiber length to be between 40 and 256 mm, to end on the cortical surface and to stay in the white matter. 

# Authors

* [**Corentin Mercier**](https://perso.telecom-paris.fr/comercier/)
* [**Sylvain Rousseau**](https://perso.telecom-paris.fr/srousseau/) 
* [**Isabelle Bloch**](https://perso.telecom-paris.fr/bloch/)
* [**Pietro Gori**](https://perso.telecom-paris.fr/pgori/)
* [**Tamy Boubekeur**](https://perso.telecom-paris.fr/boubek)

# License

Source code provided under the MIT license, please refer to the [LICENSE](LICENSE) file.

All right reserved. The Authors
